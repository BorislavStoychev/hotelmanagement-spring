package com.isoft.intern.hotelmanagementspring.controller;

import com.isoft.intern.hotelmanagementspring.model.Hotel;
import com.isoft.intern.hotelmanagementspring.service.impl.HotelService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

/**
 * @author Borislav Stoychev
 */

@RestController
@RequestMapping(
        value = "/hotels",
        consumes = APPLICATION_JSON_UTF8_VALUE,
        produces = APPLICATION_JSON_UTF8_VALUE)
public class HotelController
{
    private final HotelService hotelService;

    public HotelController(HotelService hotelService)
    {
        this.hotelService = hotelService;
    }

    @GetMapping(value = "/get")
    public Hotel get(@RequestParam final Integer id)
    {
       return hotelService.getById(id);
    }

    @GetMapping(value = "/getAll")
    public List<Hotel> getAll()
    {
        return hotelService.getAll();
    }

    @PostMapping(value = "/save")
    public int save(@RequestBody final Hotel hotelToSave)
    {
        return hotelService.save(hotelToSave);
    }

    @PutMapping(value = "/update")
    public int update(@RequestBody final Hotel hotelToUpdate)
    {
        return hotelService.update(hotelToUpdate);
    }

    @DeleteMapping(value = "/delete")
    public int delete(@RequestParam final Integer id)
    {
        return hotelService.delete(id);
    }
}
