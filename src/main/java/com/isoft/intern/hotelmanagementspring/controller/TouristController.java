package com.isoft.intern.hotelmanagementspring.controller;

import com.isoft.intern.hotelmanagementspring.model.Tourist;
import com.isoft.intern.hotelmanagementspring.service.impl.TouristService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

/**
 * @author Borislav Stoychev
 */

@RestController
@RequestMapping(
        value = "/tourists",
        consumes = APPLICATION_JSON_UTF8_VALUE,
        produces = APPLICATION_JSON_UTF8_VALUE)
public class TouristController
{
    private final TouristService touristService;

    public TouristController(TouristService touristService)
    {
        this.touristService = touristService;
    }

    @GetMapping(value = "/get")
    public Tourist get(@RequestParam final Integer id)
    {
        return touristService.getById(id);
    }

    @GetMapping(value = "/getAll")
    public List<Tourist> getAll()
    {
        return touristService.getAll();
    }

    @PostMapping(value = "/save")
    public int save(@RequestBody final Tourist touristToSave)
    {
        return touristService.save(touristToSave);
    }

    @PutMapping(value = "/update")
    public int update(@RequestBody final Tourist touristToUpdate)
    {
        return touristService.update(touristToUpdate);
    }

    @DeleteMapping(value = "/delete")
    public int delete(@RequestParam final Integer id)
    {
        return touristService.delete(id);
    }
}
