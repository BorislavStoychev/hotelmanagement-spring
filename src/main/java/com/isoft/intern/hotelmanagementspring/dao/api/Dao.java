package com.isoft.intern.hotelmanagementspring.dao.api;

import java.util.List;

/**
 * A DAO interface for CRUD operations.
 * @author Borislav Stoychev
 */
public interface Dao<T,K>
{
    /**
     * @param id
     * @return the object with the given id
     */
    T getById(K id);

    /**
     * @return all the objects in the table
     */
    List<T> getAll();

    /**
     *
     * @param t the object to save
     */
    int save(T t);

    /**
     * @param t the object to update
     */
    int update(T t);

    /**
     * @param id the object's id to delete
     */
    int delete(K id);
}