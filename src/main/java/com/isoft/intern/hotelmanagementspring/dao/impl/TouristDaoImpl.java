package com.isoft.intern.hotelmanagementspring.dao.impl;

import com.isoft.intern.hotelmanagementspring.dao.api.Dao;
import com.isoft.intern.hotelmanagementspring.model.Tourist;
import com.isoft.intern.hotelmanagementspring.util.TouristRowMapper;
import org.apache.log4j.Logger;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Borislav Stoychev
 */

@Repository
public class TouristDaoImpl implements Dao <Tourist, Integer>
{
    private JdbcTemplate jdbcTemplate;

    private static final String INSERT_TOURIST = "INSERT INTO \"Hotels\".tourists(hotel_id, first_name, last_name, age) VALUES (?, ?, ?, ?);";
    private static final String SELECT_TOURIST_BY_ID = "SELECT tourist_id, hotel_id, first_name, last_name, age FROM \"Hotels\".tourists WHERE tourist_id =?;";
    private static final String SELECT_ALL_TOURISTS = "SELECT * FROM \"Hotels\".tourists;";
    private static final String DELETE_TOURIST = "DELETE FROM \"Hotels\".tourists WHERE tourist_id = ?;";
    private static final String UPDATE_TOURIST = "UPDATE \"Hotels\".tourists SET hotel_id = ?, first_name = ?, last_name = ?, age = ? WHERE tourist_id = ?;";

    private static final Logger LOGGER = Logger.getLogger(HotelDaoImpl.class);

    public TouristDaoImpl(JdbcTemplate jdbcTemplate)
    {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Tourist getById(Integer id)
    {
        try
        {
            return jdbcTemplate.queryForObject(SELECT_TOURIST_BY_ID, new TouristRowMapper(), id);
        }
        catch (EmptyResultDataAccessException e)
        {
            LOGGER.error(e);
        }
        return null;
    }

    @Override
    public List<Tourist> getAll()
    {
        try
        {
            return jdbcTemplate.query(SELECT_ALL_TOURISTS, new TouristRowMapper());
        }
        catch (EmptyResultDataAccessException e)
        {
            LOGGER.error(e);
        }
        return null;
    }

    @Override
    public int save(Tourist touristToSave)
    {
        int status = jdbcTemplate.update(INSERT_TOURIST,
                new Object[] { touristToSave.getHotelId(), touristToSave.getFirstName(), touristToSave.getLastName(), touristToSave.getAge() });
        LOGGER.info(status == 0 ? "Error in saving the tourist!" : "Tourist is successfully saved!");
        return status;
    }

    @Override
    public int update(Tourist touristToUpdate)
    {
        int status = jdbcTemplate.update(UPDATE_TOURIST,
                new Object[] { touristToUpdate.getHotelId(), touristToUpdate.getFirstName(), touristToUpdate.getLastName(), touristToUpdate.getAge(), touristToUpdate.getTouristId() });
        LOGGER.info(status == 0 ? "Error in updating the tourist!" : "Tourist is successfully updated!");
        return status;
    }

    @Override
    public int delete(Integer id)
    {
        int status = jdbcTemplate.update(DELETE_TOURIST,
                new Object[] { id });
        LOGGER.info(status == 0 ? "Error in deleting the tourist!" : "Tourist is successfully deleted!");
        return status;
    }
}
