package com.isoft.intern.hotelmanagementspring.dao.impl;

import com.isoft.intern.hotelmanagementspring.dao.api.Dao;
import com.isoft.intern.hotelmanagementspring.model.Hotel;
import com.isoft.intern.hotelmanagementspring.util.HotelRowMapper;
import org.apache.log4j.Logger;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Borislav Stoychev
 */

@Repository
public class HotelDaoImpl implements Dao <Hotel, Integer>
{
    private JdbcTemplate jdbcTemplate;

    private static final String INSERT_HOTEL = "INSERT INTO \"Hotels\".hotels(hotel_name, price_per_night, address) VALUES (?, ?, ?);";
    private static final String SELECT_HOTEL_BY_ID = "SELECT hotel_id, hotel_name, price_per_night, address FROM \"Hotels\".hotels WHERE hotel_id = ?;";
    private static final String SELECT_ALL_HOTELS = "SELECT * FROM \"Hotels\".hotels;";
    private static final String DELETE_HOTEL = "DELETE FROM \"Hotels\".hotels WHERE hotel_id = ?;";
    private static final String UPDATE_HOTEL = "UPDATE \"Hotels\".hotels SET hotel_name = ?, price_per_night = ?, address = ? WHERE hotel_id = ?;";

    private static final Logger LOGGER = Logger.getLogger(HotelDaoImpl.class);

    public HotelDaoImpl(JdbcTemplate jdbcTemplate)
    {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Hotel getById(Integer id)
    {
        try
        {
            return jdbcTemplate.queryForObject(SELECT_HOTEL_BY_ID, new HotelRowMapper(), id);
        }
        catch(EmptyResultDataAccessException e)
        {
            LOGGER.error(e);
        }
        return null;
    }

    @Override
    public List<Hotel> getAll()
    {
        try
        {
            return jdbcTemplate.query(SELECT_ALL_HOTELS, new HotelRowMapper());
        }
        catch(EmptyResultDataAccessException e)
        {
            LOGGER.error(e);
        }
        return null;
    }

    @Override
    public int save(Hotel hotelToSave)
    {
        int status = jdbcTemplate.update(INSERT_HOTEL,
                new Object[] { hotelToSave.getHotelName(), hotelToSave.getPricePerNight(), hotelToSave.getAddress() });
        LOGGER.info(status == 0 ? "Error in saving the hotel!" : "Hotel is successfully saved!");
        return status;
    }

    @Override
    public int update(Hotel hotelToUpdate)
    {
        int status = jdbcTemplate.update(UPDATE_HOTEL,
                new Object[] { hotelToUpdate.getHotelName(), hotelToUpdate.getPricePerNight(), hotelToUpdate.getAddress(), hotelToUpdate.getHotelId() });
        LOGGER.info(status == 0 ? "Error in updating the hotel!" : "Hotel is successfully updated!");
        return status;
    }

    @Override
    public int delete(Integer id)
    {
        int status = jdbcTemplate.update(DELETE_HOTEL,
                new Object[] { id });
        LOGGER.info(status == 0 ? "Error in deleting the hotel!" : "Hotel is successfully deleted!");
        return status;
    }
}
