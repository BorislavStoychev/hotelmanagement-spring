package com.isoft.intern.hotelmanagementspring.util;

import com.isoft.intern.hotelmanagementspring.model.Tourist;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * A class which maps a tourist.
 * @author Borislav Stoychev
 */
public class TouristRowMapper implements RowMapper<Tourist>
{
    @Override
    public Tourist mapRow(ResultSet rs, int rowNum) throws SQLException
    {
        Tourist tourist = new Tourist();
        tourist.setTouristId(rs.getInt("tourist_id"));
        tourist.setHotelId(rs.getInt("hotel_id"));
        tourist.setFirstName(rs.getString("first_name"));
        tourist.setLastName(rs.getString("last_name"));
        tourist.setAge(rs.getInt("age"));
        return tourist;
    }
}
