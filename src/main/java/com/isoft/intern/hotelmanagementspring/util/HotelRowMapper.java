package com.isoft.intern.hotelmanagementspring.util;

import com.isoft.intern.hotelmanagementspring.model.Hotel;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * A class which maps a hotel.
 * @author Borislav Stoychev
 */
public class HotelRowMapper implements RowMapper<Hotel>
{
    @Override
    public Hotel mapRow(ResultSet rs, int rowNum) throws SQLException
    {
        Hotel hotel = new Hotel();
        hotel.setHotelId(rs.getInt("hotel_id"));
        hotel.setHotelName(rs.getString("hotel_name"));
        hotel.setPricePerNight(rs.getDouble("price_per_night"));
        hotel.setAddress(rs.getString("address"));
        return hotel;
    }
}
