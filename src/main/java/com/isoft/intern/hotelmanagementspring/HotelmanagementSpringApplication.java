package com.isoft.intern.hotelmanagementspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan("com.isoft.intern.hotelmanagementspring")
@SpringBootApplication
public class HotelmanagementSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(HotelmanagementSpringApplication.class, args);
	}

}
