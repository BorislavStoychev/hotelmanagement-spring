package com.isoft.intern.hotelmanagementspring.service.impl;

import com.isoft.intern.hotelmanagementspring.dao.api.Dao;
import com.isoft.intern.hotelmanagementspring.model.Hotel;
import com.isoft.intern.hotelmanagementspring.service.api.IService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Borislav Stoychev
 */

@Service
public class HotelService implements IService<Hotel, Integer>
{
    private final Dao <Hotel, Integer> hotelDao;

    public HotelService(Dao <Hotel, Integer> hotelDao)
    {
        this.hotelDao = hotelDao;
    }

    public Hotel getById(Integer id)
    {
        return hotelDao.getById(id);
    }

    public List<Hotel> getAll()
    {
        return hotelDao.getAll();
    }

    public int save(Hotel hotelToSave)
    {
        return hotelDao.save(hotelToSave);
    }

    public int update(Hotel hotelToUpdate)
    {
        return hotelDao.update(hotelToUpdate);
    }

    public int delete(Integer id)
    {
        return hotelDao.delete(id);
    }
}
