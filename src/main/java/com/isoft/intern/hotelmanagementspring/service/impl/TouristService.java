package com.isoft.intern.hotelmanagementspring.service.impl;

import com.isoft.intern.hotelmanagementspring.dao.api.Dao;
import com.isoft.intern.hotelmanagementspring.model.Tourist;
import com.isoft.intern.hotelmanagementspring.service.api.IService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Borislav Stoychev
 */

@Service
public class TouristService implements IService <Tourist, Integer>
{
    private final Dao <Tourist, Integer> touristDao;

    public TouristService(Dao <Tourist, Integer> touristDao)
    {
        this.touristDao = touristDao;
    }

    public Tourist getById(Integer id)
    {
        return touristDao.getById(id);
    }

    public List<Tourist> getAll()
    {
        return touristDao.getAll();
    }

    public int save(Tourist touristToSave)
    {
        return touristDao.save(touristToSave);
    }

    public int update(Tourist touristToUpdate)
    {
        return touristDao.update(touristToUpdate);
    }

    public int delete(Integer id)
    {
        return touristDao.delete(id);
    }
}
