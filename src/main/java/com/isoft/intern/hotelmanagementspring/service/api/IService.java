package com.isoft.intern.hotelmanagementspring.service.api;

import java.util.List;

public interface IService<T,K>
{
    /**
     * @param id
     * @return the object with the given id
     */
    T getById(K id);

    /**
     * @return all the objects in the table
     */
    List<T> getAll();

    /**
     *
     * @param t the object to save
     */
    int save(T t);

    /**
     * @param t the object to update
     */
    int update(T t);

    /**
     * @param id the object's id to delete
     */
    int delete(K id);
}
