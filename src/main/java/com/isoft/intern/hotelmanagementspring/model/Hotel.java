package com.isoft.intern.hotelmanagementspring.model;

/**
 * A hotel entity.
 * @author Borislav Stoychev
 */
public class Hotel
{
    private int hotelId;
    private String hotelName;
    private double pricePerNight;
    private String address;

    public Hotel()
    {}

    public Hotel(int hotelID, String hotelName, double pricePerNight, String address)
    {
        this.hotelId = hotelID;
        this.hotelName = hotelName;
        this.pricePerNight = pricePerNight;
        this.address = address;
    }

    @Override
    public String toString()
    {
        return "\nHotel{" +
                "hotelId=" + hotelId +
                ", hotelName='" + hotelName + '\'' +
                ", pricePerNight=" + pricePerNight +
                ", address='" + address + '\'' +
                '}';
    }

    public int getHotelId()
    {
        return hotelId;
    }

    public void setHotelId(int hotelId)
    {
        this.hotelId = hotelId;
    }

    public String getHotelName()
    {
        return hotelName;
    }

    public void setHotelName(String hotelName)
    {
        this.hotelName = hotelName;
    }

    public double getPricePerNight()
    {
        return pricePerNight;
    }

    public void setPricePerNight(double pricePerNight)
    {
        this.pricePerNight = pricePerNight;
    }

    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }
}
