package com.isoft.intern.hotelmanagementspring.model;

/**
 * A tourist entity.
 * @author Borislav Stoychev
 */
public class Tourist
{
    private int touristId;
    private int hotelId;
    private String firstName;
    private String lastName;
    private int age;

    public Tourist ()
    {}

    public Tourist(int touristID, int hotelID, String firstName, String lastName, int age)
    {
        this.touristId = touristID;
        this.hotelId = hotelID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    @Override
    public String toString()
    {
        return "Tourist{" +
                "touristId=" + touristId +
                ", hotelId=" + hotelId +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                '}';
    }

    public int getTouristId()
    {
        return touristId;
    }

    public void setTouristId(int touristId)
    {
        this.touristId = touristId;
    }

    public int getHotelId()
    {
        return hotelId;
    }

    public void setHotelId(int hotelId)
    {
        this.hotelId = hotelId;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public int getAge()
    {
        return age;
    }

    public void setAge(int age)
    {
        this.age = age;
    }
}
